#pragma once
#ifndef APPLICATION_H
#define APPLICATION_H

#include <deque>
#include "Dice.h"

using namespace std;

typedef deque<Dice*> DiceCollection;

class Application {
	public:
		~Application(void);
		static Application* GetInstance();

		void update(float dt);
		void draw();

		CollisionPlane plane;

	private:
		Application(void);
		DiceCollection _dices;
		static Application* _INSTANCE;
};

#endif // APPLICATION_H