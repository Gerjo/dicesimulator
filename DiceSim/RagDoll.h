#pragma once

#include <cyclone\cyclone.h>
#include <vector>

using namespace std;
using namespace cyclone;

class RagDoll
{
public:
    RagDoll(void);
    ~RagDoll(void);
public: //variables
    vector<ParticleCable*> cables;
    vector<ParticleCableConstraint*> constraints;
    vector<ParticleRod*> rods;
    vector<Particle*> particles;
    ParticleWorld* foo;
};

