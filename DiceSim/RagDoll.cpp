#include "RagDoll.h"


RagDoll::RagDoll(void)
{
    foo = new ParticleWorld(20);
    
    Particle* front;
    for(int i = 1 ; i <= 2; ++i){
        particles.push_back(new Particle());
        front = particles.front();
        front->setPosition(20*i,20*i,0);
        front->setVelocity(0,0,0);
        front->setDamping(0.9f);
        front->setAcceleration(Vector3::GRAVITY);
        front->clearAccumulator();
    }
    cables.push_back(new ParticleCable());
    cables.front()->particle[0] = particles.at(0);
    cables.front()->particle[1] = particles.at(1);
    cables.front()->maxLength = 1.9f;
    cables.front()->restitution = 0.3f;
    foo->getContactGenerators().push_back(cables.front());
}


RagDoll::~RagDoll(void)
{
}
