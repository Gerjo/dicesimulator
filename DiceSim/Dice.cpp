#include "Dice.h"

Dice::Dice(void) {
	tmp = 0;

	body.addRotation(Vector3(0, 1, 1));
	
	body.setLinearDamping(0.95f);
    body.setAngularDamping(0.1f);
    body.clearAccumulators();
    body.setAcceleration(cyclone::Vector3::GRAVITY);

    body.setCanSleep(false);
    body.setAwake();
	body.calculateDerivedData();
}

// No GL calls here.
void Dice::update(float dt) {
	
}

void Dice::draw() {
	tmp += 0.1f;

	

	GLfloat mat[16];
    body.getGLTransform(mat);

	body.integrate(0.0016);
	body.calculateDerivedData();

	//cout << body.getPosition().z << " " << body.getPosition().z << endl;

	glPushMatrix();
		//glRotatef(tmp, 0.0f, 1.0f, 0.0f);
		//glRotatef(tmp, 0.0f, 0.0f, 1.0f);

		//glTranslatef(0.0f, 0.0f, 0.0f);

		glMultMatrixf(mat);
		glColor3f(1, 0, 0);
		glutWireCube(1.0f);

		//glColor3f(0, 1, 0);
		//glutSolidCube(1.0f);
	glPopMatrix();

}