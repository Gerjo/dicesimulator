#include "Application.h"

// Static
Application* Application::_INSTANCE = 0;

// Static
Application* Application::GetInstance() {
	if(Application::_INSTANCE == 0) {
		Application::_INSTANCE = new Application();
	}

	return Application::_INSTANCE;
}

Application::Application(void) {
	plane.direction = Vector3(0, 1, 0);
    plane.offset	= 0;


	_dices.push_back(new Dice());
}

Application::~Application(void) {
	DiceCollection::iterator itDice;

	for(itDice = _dices.begin(); itDice != _dices.end(); ++itDice) {
		delete (*itDice);
	}
}


void Application::update(float dt) {
	DiceCollection::iterator itDice;

	for(itDice = _dices.begin(); itDice != _dices.end(); ++itDice) {
		(*itDice)->update(dt);
	}
}

void Application::draw() {

	// Draw some scale circles
    glColor3f(0.75, 0.75, 0.75);
    for (unsigned i = 1; i < 20; i++)
    {
        glBegin(GL_LINE_LOOP);
        for (unsigned j = 0; j < 32; j++)
        {
            float theta = 3.1415926f * j / 16.0f;
            glVertex3f(i*cosf(theta),0.0f,i*sinf(theta));
        }
        glEnd();
    }

	DiceCollection::iterator itDice;

	for(itDice = _dices.begin(); itDice != _dices.end(); ++itDice) {
		(*itDice)->draw();
	}
}