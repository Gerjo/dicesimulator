
// So including windows fixes a bunch of glut errors? -- Gerjo
#include <Windows.h>
#include <gl\glut.h>
#include <cyclone\cyclone.h>
#include <iostream>
#include <stdio.h>

#include "Application.h"

using namespace std;

void draw(void) {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	gluPerspective(45, 1, 0.1, 10000); // viewangle, ratio, near view, far view

	gluLookAt(
		4, 2, -1.5f,
		0, 0, 0,
		0, 1, 0
		);

	glClearColor(0.749f, 0.671f, 0.624f, 0.0f);

	Application::GetInstance()->draw();

    glutSwapBuffers();

	Sleep(10);
}

int main(int argc, char **argv) {

	// init GLUT and create Window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100,100);
	glutInitWindowSize(620,620);
	
	//gluPerspective(45, 1, 0.1, 10000);

	glutCreateWindow("Meh.");

	// register callbacks
	glutDisplayFunc(draw);
	glutIdleFunc(draw);

	// enter GLUT event processing cycle
	glutMainLoop();

	return 1;
}