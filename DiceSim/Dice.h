#pragma once
#ifndef DICE_H
#define DICE_H

#include <iostream>
#include <Windows.h>
#include "gl\glut.h"
#include "cyclone\cyclone.h"

using namespace cyclone;
using namespace std;

class Dice {
	public:
		Dice(void);

		void update(float dt);
		void draw();

		RigidBody body;
	private:
		
		GLfloat tmp;
};

#endif // DICE_H